const fs = require('fs');
const zipFolder = require('zip-folder');

class custom_set {
  constructor(icons_config, time_stamp){
    this.icons_config = icons_config;
    this.time_stamp = time_stamp;
    this.eos_icons = icons_config.eos_icons;
    this.extended_icons = icons_config.extended_icons;
  }

  grunt_cd_icons(){
    var command = '';
    try {
      for (var i = 0; i < this.extended_icons.length; i++) {
        command += (`--extended_src=svg/${this.extended_icons[i]}.svg `);
      }
      console.log('✅Generated Extended set')
      return command;
    }
    catch (err) {
      console.log('⛔️Some error occurred while generating Extended set command');
      return '';
    }
  }

  command_final(){
    var grunt_cd_icons = this.grunt_cd_icons();
    return `grunt --dist=${this.time_stamp} ${grunt_cd_icons}`;
  }

  generate_files(){
    //Generating icons_config.json file
    fs.writeFile(`${__dirname}/temp/dist_${this.time_stamp}/icons_config.json`, JSON.stringify(this.icons_config), function(err, result) {
      if(err) {
        console.log('⛔️Some error occurred while generating icons_config.json: ', err);
      }
    });

     //making zip file of dist
    zipFolder(`${__dirname}/temp/dist_${this.time_stamp}`, `${__dirname}/temp/dist_${this.time_stamp}.zip`, function(err) {
      if(err) {
        console.log('⛔️Some error occurred while generating zip file:', err);
      }
    });
  }
}

module.exports = custom_set;
