const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const cmd = require('node-cmd');
const app = express();
const generateCustomSet = require('./generate-custom')
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(`${__dirname}/dist`));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", function(req, res){
  res.send('✅ Server running. <br><br> Run eos-icons-landing to test it. <br>Repo https://gitlab.com/SUSE-UIUX/eos-icons-landing')
});

//download path for get request
app.get('/download', function(req, res){
  var ts = req.query.ts;
  var dist = `dist_${ts}`;
  var file = `${__dirname}/temp/${dist}.zip`
  res.download(file);
});

app.post("/iconsapi", function(req, res){
  //Icon names through api req
  icons_config = req.body.icons_config;

  //Timestamp at time of user clicks
  time_stamp = Math.floor(Date.now());

  //creating instance of class
  custom_set = new generateCustomSet(icons_config, time_stamp);

  //generating final command
  grunt_command = custom_set.command_final();
  console.log(grunt_command);

  cmd.get(
    grunt_command,
    function(err, data, stderr){
      custom_set.generate_files();
        res.send(`${time_stamp}`);
    }
  );
});

app.listen(port, function () {
  console.log("🤖 Server started \n👉 localhost:3000");
});
